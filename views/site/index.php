<?php

use yii\grid\GridView;
/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de selección</h1>
    </div>
    <h3>Puertos con altura menor a 1000 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó</h3>
    <?=GridView::widget([
        'dataProvider'=> $consulta1,
        'columns'=>$campos1,
    ]);
    ?>
    <h3>Etapas no circulares mostrando solo el número de etapa y la longitud de las mismas</h3>
    <?=GridView::widget([
        'dataProvider'=> $consulta2,
        'columns'=>$campos2,
    ]);
    ?>
    <h3>Puertos con altura mayor a 1500 metros figurando el nombre del puerto y el dorsal del ciclista que lo ganó</h3>
    <?=GridView::widget([
        'dataProvider'=> $consulta3,
        'columns'=>$campos3,
    ]);
    ?>
           
</div>
